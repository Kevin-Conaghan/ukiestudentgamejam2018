﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelection : MonoBehaviour {

	public int sceneIndexLow;
	public int SceneIndexHigh;

	void awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public enum BIOMES
	{
		SPRING,
		WINTER
	}

	public static BIOMES current = BIOMES.WINTER;

	public static BIOMES next = BIOMES.SPRING;

	public static int biomeChange;

	public void PickOne()
	{
		int magic = Random.Range(1, 100);
		magic %= SceneIndexHigh;
		magic += sceneIndexLow;
		SceneManager.LoadScene(magic);
	}

	void Update()
	{
		biomeChange++;
		if (biomeChange == 200)
		{
			biomeChange = 0;
			BIOMES temp;
			temp = current;
			current = next;
			next = temp;
		}
		
	}

	public void Restart()
	{
		SceneManager.LoadScene(0);
	}
}

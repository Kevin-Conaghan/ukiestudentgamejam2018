﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObject : MonoBehaviour {

	public SceneSelection.BIOMES biome;
    public bool isEnd;

    private Player_Movement playerMov;
    private GameObject player;

	public Sprite winterSprite;
	public Sprite springSprite;

	void Update()
	{
		if (FindObjectOfType<Camera>().WorldToScreenPoint(gameObject.transform.position).x > (FindObjectOfType<Camera>().pixelWidth/200)*(200-SceneSelection.biomeChange))
		{
			biome = SceneSelection.current;
		}
		else 
		{
			biome = SceneSelection.next;
		}
		
		if (biome == SceneSelection.BIOMES.WINTER)
		{
			gameObject.GetComponent<SpriteRenderer>().sprite = winterSprite;
		}
		else if (biome == SceneSelection.BIOMES.SPRING)
		{
			gameObject.GetComponent<SpriteRenderer>().sprite = springSprite;
		}
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isEnd)
        {
            if (collision.gameObject.tag == "Player")
            {
              FindObjectOfType<SceneSelection>().PickOne();
			   /* player = collision.gameObject;
                playerMov = player.GetComponent<Player_Movement>();

                Debug.Log("HEAWLLS!");
                playerMov.isEnd = true;

                player.transform.position = new Vector2(0.0f, 0.0f);
*/
                isEnd = false;
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {

    public enum GAMESTATE
    {
        start,
        Playing,
        End,
        Dead
    };

    public GAMESTATE currState = GAMESTATE.Playing;
    private bool isEnd;
    public ModuleLoader moduleLoader;

    private PlayerScript playerScript;
    private Player_Movement pm;
    private float time;
    public float deathTime;

	public SceneSelection sceneSelect;

    public static float timeLeft;

    public Text scoreText;

    

    // Use this for initialization
    void Start ()
    {
        currState = GAMESTATE.Playing;
		Instantiate(sceneSelect, new Vector2(0.0f, 0.0f), Quaternion.identity);

        //Instantiate(timer, UI.GetComponentInChildren<Text>().transform.position, Quaternion.identity);
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
        pm = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Movement>();


    }

    // Update is called once per frame
    void Update ()
	{
        switch (currState)
        {
            case GAMESTATE.start:
                break;
            case GAMESTATE.Playing:

                scoreText.text = "Score: " + pm.GetScore().ToString();

                if (playerScript.GetIsEnd() == true)
                {
                    currState = GAMESTATE.End;
                }
                if (playerScript.GetIsDead() == true)
                {
                    currState = GAMESTATE.Dead;
                }            
                break;
            case GAMESTATE.End:
               // moduleLoader.Selector();
                currState = GAMESTATE.Playing;
                break;
            case GAMESTATE.Dead:
                time += Time.deltaTime;

                if (time > deathTime)
                {
                   /* player.transform.position = new Vector2(0.0f, 0.0f);
                    playerScript = player.GetComponent<PlayerScript>();
                    currState = GAMESTATE.Playing;
                    playerScript.SetIsDead(false);
                    time = 0.0f;*/
                    timeLeft = 0;
                    sceneSelect.GetComponent<SceneSelection>().Restart();
                }
                break;  
        }
	}
}

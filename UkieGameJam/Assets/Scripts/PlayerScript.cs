﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public  bool isDead;
    public  bool isEnd;
    public Vector2 startPos;

	private Animator playerAnim;

	// Use this for initialization
	void Start ()
    {
		playerAnim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {

	}




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Spikes")
        {
            isDead = true;
			playerAnim.SetBool("isDead", isDead);
        }
    }

    public bool GetIsDead()
    {
        return isDead;
    }
    public bool GetIsEnd()
    {
        return isEnd;
    }

    public void SetIsDead(bool isdead)
    {
        isDead = isdead;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Change : MonoBehaviour {

    private SpriteRenderer sr;
	private Rigidbody2D rb;
	private Player_Movement pm;
	private Animator playerAnim;

	public int currPlayerType;

    private int playerCount;

	public float lightJumpHeight;
	public int lightHorizJump;
	public int lightMoveSpeed;

	public float heavyJumpHeight;
	public int heavyHorizJump;
	public int heavyMoveSpeed;

	// Use this for initialization
	void Start ()
    {
        sr =  gameObject.GetComponent<SpriteRenderer>();
		rb = gameObject.GetComponent<Rigidbody2D>();
		pm = gameObject.GetComponent<Player_Movement>();
		playerAnim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.Joystick1Button3))
        {

			playerAnim.SetBool("hasPressed", true);
			currPlayerType++;

			if(currPlayerType > 1)
			{
				currPlayerType = 0;
			}
		}


		if(currPlayerType == 0)
		{
			playerAnim.SetBool("isGhost", false);
			playerAnim.SetBool("isHeavy", true);
			rb.mass = 1.0f;
			pm.SetJump(heavyJumpHeight);
			pm.SetHorizontalJump(heavyHorizJump);
			rb.gravityScale = 1.0f;
            pm.SetSpeed(heavyMoveSpeed);
		}

		if(currPlayerType == 1)
		{
			playerAnim.SetBool("isHeavy", false);
			playerAnim.SetBool("isGhost", true);
			rb.mass = 0.5f;
			pm.SetJump(lightJumpHeight);
			pm.SetHorizontalJump(lightHorizJump);
			rb.gravityScale = 0.5f;
			pm.SetSpeed(lightMoveSpeed);
		}
	}

	public int GetCurrPlayerType()
	{
		return currPlayerType;
	}

}

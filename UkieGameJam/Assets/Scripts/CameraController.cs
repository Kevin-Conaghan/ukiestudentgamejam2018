﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Vector2 target;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float translateX = target.x * Time.deltaTime;
        float translateY = target.y * Time.deltaTime;


        gameObject.transform.Translate(new Vector3(translateX, translateY, 0.0f));
	}


}

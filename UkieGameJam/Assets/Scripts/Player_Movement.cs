﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Movement : MonoBehaviour {

    //Jump variables
    public int maxJumpCount;
    private int jumpCount;
    public float jump;
    public float jumpMultiplier;

    //friction variables
    private float direction;
    Vector2 normalisedDirection;
    public float frictionX;
    public float frictionY;

    private PlayerScript playerScript;

	public bool isGrounded;

    //movement speed 
    public int speed;
	private int horizJump;

    public bool isEnd;

    private static int score;
    public Text scoreText;


    private bool isTriggering;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
	private Player_Change pc;
	private Animator playerAnim;

    public AudioClip jumpClip;



    // Use this for initialization
    void Start()
    {
        //get the player's sprite and rigidbody2D components
        sr = gameObject.GetComponent<SpriteRenderer>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        playerScript = gameObject.GetComponent<PlayerScript>();
		pc = gameObject.GetComponent<Player_Change>();
		playerAnim = gameObject.GetComponent<Animator>();
        //set the jump count to 0
        jumpCount = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
   	  //if we are grounded reset the jump count
   	   	if (collision.gameObject.tag == "platform")
	    {
			isGrounded = true;
           	jumpCount = 0;
       	}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
		isGrounded = true;     

        if (collision.gameObject.tag == "collectable")
        {
            Destroy(collision.gameObject);
            score++;

            scoreText.text = "Score: " + score.ToString();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "goo")
        {
            //after we exit the goo reset the players jump mechanics
            isTriggering = false;
            jump = jump / jumpMultiplier;
            jumpCount = 0;
            //add a force in the opposite direction of the goo to further assist the jump
            rb.AddForce(new Vector2(-4.0f * normalisedDirection.x, 0.0f), ForceMode2D.Impulse);
        }
    }

	void onCollisionExit2D(Collision2D colilision)
	{
		isGrounded = false;
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        //player movement functions
        if (playerScript.GetIsDead() != true)
        {
            Movement();
			//update the speed variable for the animation changes
			if(rb.velocity.magnitude <= 0)
			{
				playerAnim.SetFloat("speed", 0.0f);
			}
        }
        //while we are in the goo
        if (isTriggering)
        {
            //add friction to the player so he doesn't fall
            AddFriction();
        }
    }

	void Update()
	{
		if (playerScript.GetIsDead() != true)
		{
			Jump();
		}
	}
    void Movement()
    {
		//calculate the speed with delta time
		float movSpeed = speed * Time.deltaTime;

        if (Input.GetButton("Horizontal"))
		{
			//Move Right
			if (Input.GetKey(KeyCode.D))
			{
				rb.AddForce(Vector2.right * movSpeed);

				if(pc.GetCurrPlayerType() == 0)
				{
					playerAnim.SetFloat("speed", Mathf.Clamp( Mathf.Abs(rb.velocity.magnitude), 0.0f, 1.0f));
				}

				if(pc.GetCurrPlayerType() == 1)
				{
					playerAnim.SetFloat("speed", Mathf.Clamp( Mathf.Abs(rb.velocity.magnitude), 0.0f, 1.0f));
				}
			}
        }
		
		if(Input.GetAxis("Horizontal") < 0)
		{
			if(Input.GetKey(KeyCode.A))
			{
				//Move Left
				rb.AddForce(Vector2.left * movSpeed);    

				if(pc.GetCurrPlayerType() == 0)
				{
					playerAnim.SetFloat("speed", Mathf.Clamp( Mathf.Abs(rb.velocity.magnitude), 0.0f, 1.0f));
				}
			
				if(pc.GetCurrPlayerType() == 1)
				{
					playerAnim.SetFloat("speed", Mathf.Clamp( Mathf.Abs(rb.velocity.magnitude), 0.0f, 1.0f));
				}
			}
		}
	}
	
	void Jump()
    {
        //if the player's current jump is not greater than the max jump the player can still jump
		if (Input.GetButtonDown("Jump") && isGrounded || Input.GetButtonDown("Jump") && pc.GetCurrPlayerType() == 1)
       	{
			if(jumpCount < maxJumpCount)
			{
				//calculate the jump height

				isGrounded = false;
                //apply the jump foce to the player
                AudioSource jumpSound = gameObject.GetComponent<AudioSource>();
                jumpSound.PlayOneShot(jumpClip);
				rb.AddForce((Vector2.up * jump) + Physics2D.gravity, ForceMode2D.Impulse);
            	jumpCount++;
			}
        }   
    }

    void AddFriction()
    {
        //if the goo is on the right
        if (direction > 0.0f)
        {
            //add the friction forces towards the right
            rb.AddForce(new Vector2(frictionX, frictionY));
        }
        //if the goo is on the left
        else if (direction < 0.0f)
        {
            //add the friction forces towards the left
            rb.AddForce(new Vector2(-frictionX, -frictionY));
        }
    }

    public int GetScore()
    {
        return score;
    }


	public void SetJump(float jumpHeight)
	{
		jump = jumpHeight;
	}
	public void SetSpeed(int movSpeed)
	{
		speed = movSpeed;
	}
	public void SetHorizontalJump(int horizJumps)
	{
		horizJump = horizJumps;
	}
	
}

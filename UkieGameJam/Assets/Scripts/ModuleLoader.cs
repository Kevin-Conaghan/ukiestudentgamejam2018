﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleLoader : MonoBehaviour {

	public List<Module> levelList;
	public static Stack<Module> levels;
	public Module nextUp;



    public CameraController camController;
	public Module.Seasons currSeason;
	public Module.Seasons nextSeason;
	public int changeProgress = 0;
	
	// Use this for initialization
	void Start () {
		if (levels == null)
		{
			levels = new Stack<Module>();
			foreach(Module level in levelList)
			{
				levels.Push(level);
			}
		}
		Selector();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Selector()
	{
		nextUp = levels.Peek();	
		SeasonChange();
		Shuffler();
	}

	public void SeasonChange()
	{
		if (changeProgress > 0)
		{
			changeProgress -= 25;
		}
		else
		{
			changeProgress = 100;
			currSeason = nextSeason;
			if (nextSeason == Module.Seasons.Autumn)
			{
				nextSeason = Module.Seasons.Winter;
			}
			else if (nextSeason == Module.Seasons.Spring)
			{
				nextSeason = Module.Seasons.Summer;
			}
			else if (nextSeason == Module.Seasons.Summer)
			{
				nextSeason = Module.Seasons.Autumn;
			}
			else if (nextSeason == Module.Seasons.Winter)
			{
				nextSeason = Module.Seasons.Spring;
			}
		}
	}


	private void Shuffler()
	{
		//two queues to hold the popped level modules
		Queue<Module> first = new Queue<Module>();
		Queue<Module> second = new Queue<Module>();
		//store number of level modules we have
		int x = levels.Count;
		for (int i = 0; i < x; ++i)
		{
			//move one to each queue one at a time
			if (i%2 == 0)
			{
				first.Enqueue(levels.Pop());
			}
			else
			{
				second.Enqueue(levels.Pop());
			}
		}
		//push half the holding queues to the level stack one at a time
		for (int i = 0; i < x/4; i++)
		{
			levels.Push(first.Dequeue());
		}
		for (int i = 0; i < x/4; i++)
		{
			levels.Push(second.Dequeue());
		}
		for (int i = 0; i < x/4; i++)
		{
			levels.Push(first.Dequeue());
		}
		for (int i = 0; i < x/4; i++)
		{
			levels.Push(second.Dequeue());
			
		}
	}

}
